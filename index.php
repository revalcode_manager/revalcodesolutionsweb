
<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Meta tags -->
          <meta charset="utf-8">

          <link rel="shortcut icon" type="image/x-icon" href="images/logo/icon.ico">

          <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

          <title>RevalCode Solutions Inc.</title>    
         
        <!-- Font Awesome -->
            <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">

        <!-- Google Font-->
            <link href="https://fonts.googleapis.com/css?family=Advent+Pro|Montserrat&display=swap" rel="stylesheet"> 

        <!-- Bootstrap -->
            <link href="css/bootstrap.min.css" rel="stylesheet">
        
        <!-- Material Design Bootstrap -->
            <link href="css/mdb.min.css" rel="stylesheet">

            <!-- Formulario Full Screen -->
            <link rel="stylesheet" type="text/css" href="css/normalize.css" />
            <link rel="stylesheet" type="text/css" href="css/demo.css" />
            <link rel="stylesheet" type="text/css" href="css/component.css" />
            <link rel="stylesheet" type="text/css" href="css/cs-select.css" />
            <link rel="stylesheet" type="text/css" href="css/cs-skin-boxes.css" />
            <script src="js/modernizr.custom.js"></script>
        
        <!--Style -->
            <link href="css/style.css" rel="stylesheet">

        <!--Ajax Css-->   
            <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css" />
            <link rel="stylesheet" type="text/css" href="css/modules/responsive.css">

        <!-- Hover Css -->    
            <link href="ihover/src/ihover.css" rel="stylesheet">


        


    </head>

    <body id="bodyCode" data-spy="scroll" data-offset="60" data-target="#myNavbar">
    <div id="particles-js"></div>

       <!--Loader--> 
         <div id="preloader">
            <div id="preloader-inner"></div>
         </div>

         <div class="site-overlay"></div>

    <header>

          <!-- Navbar -->
             <nav id="myNavbar" class="navbar fixed-top navbar-expand-lg navbar-light scrolling-navbar">
                <div class="container">

                    <!-- Brand -->
                      <a class="navbar-brand" href="#" target="">
                        <img src="images/logo/logo2.png" height="40px">
                      </a>

                      <!-- Collapse -->
                         <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                          <span class="navbar-toggler-icon"></span>
                         </button>

                    <!-- Links -->
                      <div class="navbar-collapse collapse " id="navbarSupportedContent">

                        <!-- Left -->
                          <ul class="navbar-nav mr-auto nav-pills">
                              <li class="nav-item ">
                                <a class="nav-link" href="#home">Home</a>
                              </li>
                              <li class="nav-item">
                                <a class="nav-link" href="#services" >Services</a>
                              </li>
                              <li class="nav-item">
                                <a class="nav-link" href="#portfolio">Portfolio</a>
                              </li>
                              <li class="nav-item">
                                <a class="nav-link" href="#hireus" >Hire Us!</a>
                              </li>
                          </ul>
                      
                      </div>
                </div>
             </nav>
           

    
    </header>        
      
  
        
        <!-- Particles Intro -->  
    <!--    <div id="particleWrapper">  
            <div id="particles-js" style="height: 764px;">
             <h2 class="layer-1 font-weight-bold">Keep It Simple</h2>
                <h3 class="layer-2 font-weight-bold">Create Awesome Websites</h3>
                <h4 class="layer-3">Helping companies to create your own business website.</h4>

            </div>
        </div>-->
  <main>
  <!-- <button id="modalActivate" type="button" class="btn btn-info botonflotante" data-toggle="modal" data-target="#modalfull">
			Contactanos
    </button> -->
    <div class="frame-content botonflotante" id="modalActivate" data-toggle="modal" data-target="#modalfull"><div class="widget-position-right sidebar-position-right"><div id="button" data-testid="widgetButton" data-toggle="tooltip" data-placement="top" title="Hire us!" class="chat-closed mobile-size__large bubbleAnimation-appear-done bubbleAnimation-enter-done"><div class="buttonWave"></div><button type="button" id="button-body" data-testid="widgetButtonBody" class=" chrome" tabindex="0" aria-label="Open chat widget" style="color:#28403D; /*rgb(0, 125, 252)*/; background: #497367;/*linear-gradient(135deg, rgb(41, 50, 60), rgb(72, 85, 99))*/ box-shadow: rgba(41, 50, 60, 0.5) 0px 4px 24px;"><i class="material-icons type1 for-closed active" style="color: rgb(255, 255, 255);"><svg id="ic_bubble" fill="#FFFFFF" height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg" aria-hidden="true"><path d="M20 2H4c-1.1 0-2 .9-2 2v18l4-4h14c1.1 0 2-.9 2-2V4c0-1.1-.9-2-2-2z"></path><path d="M0 0h24v24H0z" fill="none"></path></svg></i><i class="material-icons type2 for-closed active"><svg id="ic_create" fill="blue" height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg" aria-hidden="true"><path d="M3 17.25V21h3.75L17.81 9.94l-3.75-3.75L3 17.25zM20.71 7.04c.39-.39.39-1.02 0-1.41l-2.34-2.34c-.39-.39-1.02-.39-1.41 0l-1.83 1.83 3.75 3.75 1.83-1.83z"></path><path d="M0 0h24v24H0z" fill="none"></path></svg></i><i class="material-icons type1 for-opened " style="color: rgb(255, 255, 255);"><svg id="ic_send" fill="#FFFFFF" height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg" aria-hidden="true"><path d="M2.01 21L23 12 2.01 3 2 10l15 2-15 2z"></path><path d="M0 0h24v24H0z" fill="none"></path></svg></i><i class="material-icons type2 for-opened "><svg id="ic_send" fill="#FFFFFF" height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg" aria-hidden="true"><path d="M2.01 21L23 12 2.01 3 2 10l15 2-15 2z"></path><path d="M0 0h24v24H0z" fill="none"></path></svg></i></button></div></div></div>

		<?php require 'views/formulario.view.php';?>

      <!-- Header-->    
   
      <div class="header">

          <p class="fontadvent">Keep It</p>
          <b>
            <div class="headertext">
              Simple.<br /> 
              Responsive.<br />
              Creative.<br />
              RevalCode.<br />
              </div>
          </b>
        
        </div>

      <!--Our Services--> 
    <div class="ourconteiner services" id="services">
                       
        <div id="OurServices" class="container scrollspy-example">
        
            <section class="text-center services-section">

                <!-- Section heading -->
                <div class="services-heading">
                 
                    <h2 class="h2-responsive wow bounceInDown font-weight-bold titulo shadows-1 text-center fontadvent" data-wow-delay="0.2s">Our Services</h2>

                       <!-- Icon Divider -->
                            <div class="divider-custom">
                              <div class="divider-custom-line"></div>
                              <div class="divider-custom-icon"><i class="fas fa-code"></i></div>
                              <div class="divider-custom-line"></div>
                            </div>
                 
                </div>

                <div class="row align-items-center fontmon">
                    <div class="col-lg-6 col-md-6 services-esp">
                      <h3>E-Commerce</h3>
                       <!-- normal -->
                        <div class="ih-item square effect5 left_to_right"><a href="#">
                            <div class="img">
                              <img src="images/services/EcommerceFinal.png">
                            </div>
                            <div class="info">
                             
                              <p>Sell your products or services online. Whether you sell a physical good or a digital download product, our shopping cart makes it easy to sell online.
                                Some of the features are: Accept credit cards online;Shipping calculation;Coupons, discounts and special;Mobile optimized checkout;Manage orders and customers.</p>
                            </div></a></div>
                        <!-- end normal -->
                    </div>
                    
                     <div class="col-lg-6 col-md-6 services-esp">
                      <h3>Responsive Desing</h3>
                       <div class="ih-item square effect5 left_to_right"><a href="#">
                            <div class="img">
                              <img src="images/services/Responsive.png">
                            </div>
                            <div class="info">
                              
                              <p>Adapt your websites to any device.Today, 80% of the search is mobile, so your site must be prepared to be found on the web and be adaptable to any screen.</p>
                            </div></a></div>
                    </div>
                </div>

                  <!--Second Row-->
                <div class="row align-items-center fontmon">
                     <div class="col-lg-6 col-md-6 services-esp">
                      <h3>Landing Page</h3>
                        <div class="ih-item square effect5 left_to_right">
                          <a href="#">
                            <div class="img">
                              <video class="video-fluid z-depth-1" autoplay loop muted>
                                  <source src="videos/LandingPageFinal.mp4" type="video/mp4" />
                              </video>
                            </div>
                            <div class="info">
                              <p>Promote services and products in the most effective way.This types of sites is great to convert your marketing campaings in call to action, and generate a lot of leads for your business. Give to your clients all of information needed to know your company and much more.</p>
                            </div>
                          </a>
                        </div>
                     </div>

                     <div class="col-lg-6 col-md-6 services-esp">
                       <h3>Web Site</h3>
                        <div class="ih-item square effect5 left_to_right">
                          <a href="#">
                            <div class="img">
                              <video class="video-fluid z-depth-1" autoplay loop muted>
                                  <source src="videos/WebSiteFinal.mp4" type="video/mp4" />
                              </video>
                            </div>
                            <div class="info">
                              <p>Build a platform with several sections of your company.Detail your products and services, describe your values and mission, why and who you are and so on.Create form contacts and dive yourself in new and refreshing designs for landing pages.</p>
                            </div>
                          </a>
                        </div>
                     </div>
                </div>                               
                                
                </div>
            
            </section>
        </div>


    </div>
           

           <!--Banner Responsive Desing-->
      <!--<div id="wall_2" class="image" data-stellar-background-ratio="0.4" >
            <div class="mask flex-center wall_1">
             <div class="container">
                    <div class="row align-items-center">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 wow fadeInLeft" style="position: relative;" data-wow-delay=".1s">
                              <h3 class="">We provide the great support to our customers</h3>
                        </div>

                     
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 wow fadeInRight" style="position: relative; ">
                             <a href="#" class="btn btn-color btn-lg" >
                                HIRE US!<i class="fas fa-magic ml-1"></i>
                            </a>
                        </div>
                    </div>
              </div>
            </div>
        </div>-->
        
              
          <!--Carousel Our Values-->
       <div class="ourconteiner values" id="portfolio">
                      <section class="page-section portfolio" id="portfolio">
                          <div class="container">

                            <!-- Portfolio Section Heading -->
                              <h2 class="h2-responsive wow fadeIn font-weight-bold titulo shadows-1 text-center fontadvent" data-wow-delay="0.2s">Our Portfolio</h2>

                              <!-- Icon Divider -->
                            <div class="divider-custom">
                              <div class="divider-custom-line"></div>
                              <div class="divider-custom-icon"><i class="fas fa-code"></i></div>
                              <div class="divider-custom-line"></div>
                            </div>

                                      <div id="carousel-thumb" class="carousel slide carousel-fade carousel-thumbnails" data-ride="carousel" data-interval="5000"   data-pause="hover">
                                                  <!--Slides-->
                                                  <div class="carousel-inner" role="listbox">
                                                      <div class="carousel-item active">
                                                          <div class="portfolio-item mx-auto" data-toggle="modal" >
                                                          <div class="portfolio-item-caption d-flex align-items-center justify-content-center h-100 w-100">
                                                            <div class="portfolio-item-caption-content text-center text-white">
                                                             <a href="https://nogadev.com/"  target="_blank"> <i class="fas fa-search-plus fa-3x"></i>
                                                             </a>
                                                            </div>
                                                          </div>
                                                               <video class="video-fluid" autoplay loop muted>
                                                                 <source src="videos/NogaDev.mp4" type="video/mp4" />
                                                                </video>
                                                        </div>
                                                      </div>

                                                      <div class="carousel-item ">
                                                          <div class="portfolio-item mx-auto" data-toggle="modal">
                                                          <div class="portfolio-item-caption d-flex align-items-center justify-content-center h-100 w-100">
                                                            <div class="portfolio-item-caption-content text-center text-white">
                                                             <a href="http://mynewcorpinusa.com/"  target="_blank"> <i class="fas fa-search-plus fa-3x"></i>
                                                             </a>
                                                            </div>
                                                          </div>
                                                                <video class="video-fluid" autoplay loop muted>
                                                                 <source src="videos/Mynewcorp.mp4" type="video/mp4" />
                                                                </video>
                                                        </div>
                                                      </div>
                                                  
                                                  <!--/.Slides-->
                                                  <!--Controls-->
                                                  <a class="carousel-control-prev" href="#carousel-thumb" role="button" data-slide="prev">
                                                      <span class="iconslider carousel-control-prev-icon" aria-hidden="true"></span>
                                                      <span class="sr-only">Previous</span>
                                                  </a>
                                                  <a class="carousel-control-next" href="#carousel-thumb" role="button" data-slide="next">
                                                      <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                                      <span class="sr-only">Next</span>
                                                  </a>
                                                  <!--/.Controls-->
                                                  <ol class="carousel-indicators" style="margin-bottom: 2rem">
                                                      <li data-target="#carousel-thumb" data-slide-to="0" class="active"></li>
                                                      <li data-target="#carousel-thumb" data-slide-to="1"></li>
                                                      
                                                  </ol>
                                      </div>
                                    <!--/.Carousel Wrapper-->
                          </div>
                      </section>
                        
                  </div>
             </div>
        </div>






    <div class="social">
        <ul>
            <li>
              <a href="#" target="_blank" class="icon-face rounded-right"
              style="padding-left: 20px;"> 
              <i class="fab fa-facebook-square"></i>
              </a>
            </li>
            <li>
              <a href="#" target="_blank" class="icon-tw rounded-right"
              style="padding-left: 18px;">
              <i class="fab fa-twitter "></i>
              </a> 
              </li>
            <li>
              <a href="#" target="_blank" class="icon-link rounded-right"
              style="padding-left: 20px;">
              <i class="fab fa-linkedin"></i>
              </a>
            </li>
            <li>
              <a href="#" target="_blank" class="icon-insta rounded-right" 
              style="padding-left: 20px;">
              <i class="fab fa-instagram"></i>
            </a>
            </li>
        </ul>
    </div>

    </main>
       

     <!--Footer-->
    <footer class="page-footer text-center wow slideInUp" id="hireus">

        <hr class="my-4">
      <div class="container ">
          <!-- Social icons -->
          <div class="row text-center d-flex justify-content-center pt-5 mb-3">
            <div class="col-lg-3">
              <i class="fas fa-map-marker-alt fa-2x"></i>
              <p class="pfooter">5035 Hazel Ave B, Philadelphia, PA 19143</p>
            </div>
             <div class="col-lg-3">
              <i class="fab fa-skype fa-2x"></i>
              <p class="pfooter">+1 920 333 1106</p>
            
            </div>
             <div class="col-lg-3">
                <i class="fas fa-phone fa-2x text-center"></i>
                <br>
                <p class="text-center pfooter">+1 267 760 4881</p>
            
            </div>
             <div class="col-lg-3">
              <i class="fas fa-at fa-2x"></i>
              <p class="pfooter" >hello@revalcodesolution.com</p>
            </div>
                
          </div>
      </div>  
            <!--Copyright-->
          <div class="footer-copyright py-3 pfooter">
            © 2019 Copyright:
            <a href="https://nogadev.com/" target="_blank"> NOGADEV </a>
</div>
</footer>



  
    



  <!--SCRIPTS-->    

    <script type="text/javascript" src="js/text.js"></script>
    <!-- JQuery -->
        <script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>

    <!-- Bootstrap tooltips -->
        <script type="text/javascript" src="js/popper.min.js"></script>

    <!-- Bootstrap core JavaScript -->
        <script type="text/javascript" src="js/bootstrap.min.js"></script>
    
    <!-- MDB core JavaScript -->
        <script type="text/javascript" src="js/mdb.min.js"></script>
    
    <!-- Plugins Js -->  
        <script src="js/plugins/plugins.js"></script> 

    <!-- Assan Js -->    
        <script src="js/assan.custom.js"></script> 

    <!-- Particles.js -->    
        <script type="text/javascript" src="js/particles.js "></script>

    <!--Hero Particles -->    
        <script type="text/javascript" src="js/heroparticles.js "></script>
            
        <script>
            particlesJS.load('particles-js', 'particles.json', function() {
            console.log('callback - particles.js config loaded');
        });

           
        </script>

    <!-- Your custom JavaScript -->
	    <script type="text/javascript" src="js/main.js"></script>

    <!-- Captcha -->
      <script src="https://www.google.com/recaptcha/api.js" async defer></script>

    <!-- Formulario Full Screen -->
      <script src="js/classie.js"></script>
      <script src="js/fullscreenForm.js"></script>

    </body>
</html>
