    
<!-- banner start -->
<!-- ================ -->
<div class="mb-1">          
  <div class="colorfondo-estatico pt-5 pb-2" >
    <div class="container pt-5">
        <div class="row py-3">
          <div class="col-md-12">
            <p class="text-center medium_white">Ooops! La página no funciona</p>
          </div>
        </div>
    </div>
  </div>
</div>
<!-- banner end -->  
<!-- ================ -->
<div class="breadcrumb-container">
  <div class="container">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><i class="fa fa-home pr-2"></i><a class="link-dark" href="index.php">Home</a></li>
      <li class="breadcrumb-item active">Page 404</li>
    </ol>
  </div>
</div>
<!-- breadcrumb end -->

<!-- main-container start -->
<!-- ================ -->
<section class="main-container light-gray-bg text-center margin-clear">

  <div class="container">
    <div class="row justify-content-lg-center">

      <!-- main start -->
      <!-- ================ -->
      <div class="main col-lg-6 pv-40">
        <h1 class="page-title extra-large"><span class="text-default">404</span></h1>
        <h2 class="mt-4">Ooops! La página no funciona</h2>
        <p class="lead">Intente ingresar más tarde y si la falla persiste ponte en contacto con nosotros.</p>
        <a href="index.php" class="btn btn-default btn-animated btn-lg">Volver al Inicio <i class="fa fa-home"></i></a>
      </div>
      <!-- main end -->

    </div>
  </div>
</section>
<!-- main-container end -->