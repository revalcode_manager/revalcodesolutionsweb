<!-- Modal -->
<div class="modal fade right modal-form-estilo" id="modalfull" tabindex="-1" role="dialog" aria-labelledby="exampleModalPreviewLabel" aria-hidden="true">
    <div class="modal-dialog modal-content" id="contenedorModalFull">
        <button class="cerrar_modal" id="closeModalFull" data-dismiss="modal">&times;</button>
        <div class="fs-form-wrap" id="fs-form-wrap">
            <div class="fs-title" style="font-family: 'Advent Pro', sans-serif;">
                <h1 >Contact Form</h1>
            </div>
            <form class="fs-form fs-form-full" autocomplete="off"action="#" method="post" id="form_contacto" style="font-family: 'Montserrat', sans-serif;">
                <ol class="fs-fields">
                    <li>
                        <label class="fs-field-label fs-anim-upper" for="fullname">What's your Full Name?</label>
                        <input type="text" id="fullname" name="fullname" class="fs-anim-lower" placeholder="Full Name*" required>
                    </li>
                  
                    <li>
                        <label class="fs-field-label fs-anim-upper" for="email" data-info="We won't send you spam, we promise...">What's your email address?</label>
                        <input type="email" id="email" name="email" class="fs-anim-lower" placeholder="email@email.us*" required>
                    </li>

                    <li data-input-trigger>
                        <label class="fs-field-label fs-anim-upper" for="pais" data-info="Select your Country">What's your country?</label>
                        <div class="fs-anim-lower" tabindex="0">
                            <select class="fs-anim-lower col-12" name="pais" id="pais" required>
                                <option value='' disabled selected>Select Country</option>
                            </select>
                        </div>

                        <label class="fs-field-label fs-anim-upper mt-4" data-info="County Code, City Code and Telephone Number">What's your telephone number?</label>
                        <div class="telefonoFlex">
                            <input type="text" id="codepais" name="codepais" class="fs-mark fs-anim-lower col-md-3" placeholder='+' data-toggle="tooltip" data-placement="top" title="Country Code" required>

                            <!-- <label class="fs-field-label fs-anim-upper" for="codearea">What's your city code?</label> -->
                            <input type="number" id="codearea" name="codearea" step="0.01" class="fs-mark fs-anim-lower col-md-3" placeholder='City Code' data-toggle="tooltip" data-placement="top" title="City Code" required>

                            <!-- <label class="fs-field-label fs-anim-upper" for="telefono">What's your telephone number?</label> -->
                            <input type="number" id="telefono" name="telefono" step="0.01" class="fs-mark fs-anim-lower col-md-6" placeholder='Telephone' data-toggle="tooltip" data-placement="top" title="Telephone Number" required>
                        </div>
                    </li>

                    <!-- Seccion destinada a la seleccion de los sitios -->
                    <li data-input-trigger>
                        <label class="fs-field-label fs-anim-upper" for="prioritySite" data-info="This will help us know what kind of service you need">What's your priority for your new website?</label>
                        <div class="fs-radio-group fs-radio-custom clearfix fs-anim-lower">
                            <span><input id="q3b" name="prioritySite" type="radio" value="Landing Page"/><label for="q3b" class="radio-conversion">Landing Page</label></span>
                            <span><input id="q3c" name="prioritySite" type="radio" value="Web Site"/><label for="q3c" class="radio-social">Web Site</label></span>
                            <span><input id="q3a" name="prioritySite" type="radio" value="Market Page"/><label for="q3a" class="radio-mobile">Market Page</label></span>
                        </div>
                    </li>

                    <li>
                        <label class="fs-field-label fs-anim-upper" for="mensaje">Describe how you imagine your new website</label>
                        <textarea class="fs-anim-lower" id="mensaje" name="mensaje" placeholder='Message'></textarea>
                    </li>

                </ol><!-- /fs-fields -->

                <div>
                    <!-- Captcha -->
                    <div class="g-recaptcha" id="captcha" data-sitekey="6LfECr0UAAAAAFP42aIXDg5YwteBg_chSdThUG0I" style="display: none;"></div>
                </div>

                <!-- Cartel de error -->
                <div class="fs-message-error-end" id="fs-message-error-end" style="display: none; font-family: 'Advent Pro', sans-serif;">Please fill the field before continuing</div>
                
                <button type="submit" name="submit_form" id="submit_form" class="mail-btn">Send answers</button>
            </form><!-- /fs-form -->
        </div><!-- /fs-form-wrap -->

    </div>
</div>
<!-- Modal -->