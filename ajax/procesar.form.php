<?php
error_reporting(0);
header('Content-type: application/json; charset=utf-8');
require '../admin/config.php';
require '../funciones.php';

// Seteo de datos
if(isset($_POST['fullname'])){
    $fullname = $_POST['fullname'];
}else{
    $fullname = "";
}
if(isset($_POST['email'])){
    $email = $_POST['email'];
}else{
    $email = "";
}
if(isset($_POST['codepais'])){
    $codepais = '+' . $_POST['codepais'];
}else{
    $codepais = "";
}
if(isset($_POST['codearea'])){
    $codearea = $_POST['codearea'];
}else{
    $codearea = "";
}
if(isset($_POST['telefono'])){
    $telefono = $_POST['telefono'];
}else{
    $telefono = "";
}
if(isset($_POST['mensaje'])){
    $mensaje = $_POST['mensaje'];
}else{
    $mensaje = "";
}
if(isset($_POST['captcha'])){
     $captcha = $_POST['captcha'];
}else{
    $captcha = "";
}
if(isset($_POST['prioritySite'])){
    $prioritySite = $_POST['prioritySite'];
}else{
   $prioritySite = "";
}

//Limpieza de datos
if(!empty($fullname)){
    $limpio = limpiarString($fullname);
    $fullname = $limpio['dato'];
    if($limpio['errores'] == true){
        $validar['fullname'] = false;
    }else{
        $validar['fullname'] = true;
    }
}else{
    $validar['fullname'] = false;
}

if(!empty($email)){
    $limpio = limpiarCorreo($email);
    $email = $limpio['dato'];
    if($limpio['errores'] == true){
        $validar['email'] = false;
    }else{
        $validar['email'] = true;
    }
}else{
    $validar['email'] = false;
}

if(!empty($codepais)){
    $limpio = limpiarString($codepais);
    $codepais = $limpio['dato'];
    if($limpio['errores'] == true){
        $validar['codepais'] = false;
    }else{
        $validar['codepais'] = true;
    }
}else{
    $validar['codepais'] = false;
}

if(!empty($codearea)){
    $limpio = limpiarNum($codearea);
    $codearea = $limpio['dato'];
    if($limpio['errores'] == true){
        $validar['codearea'] = false;
    }else{
        $validar['codearea'] = true;
    }
}else{
    $validar['codearea'] = false;
}

if(!empty($telefono)){
    $limpio = limpiarNum($telefono);
    $telefono = $limpio['dato'];
    if($limpio['errores'] == true){
        $validar['telefono'] = false;
    }else{
        $validar['telefono'] = true;
    }
}else{
    $validar['telefono'] = false;
}

if(!empty($mensaje)){
    $limpio = limpiarString($mensaje);
    $mensaje = $limpio['dato'];
    if(strlen($mensaje)<250){
        $validar['mensaje'] = true;
    }else{
        $validar['mensaje'] = false;
    }
}else{
    $validar['mensaje'] = true;
}

if(!empty($captcha)){
    if(validarCaptcha($captcha)){
        $validar['captcha'] = true;
    }else{
        $validar['captcha'] = false;
    }
}else{
    $validar['captcha'] = false;
}

if(!empty($prioritySite)){
    $limpio = limpiarString($prioritySite);
    $prioritySite = $limpio['dato'];
    if($limpio['errores'] == true){
        $validar['prioritySite'] = false;
    }else{
        $validar['prioritySite'] = true;
    }
}else{
    $validar['prioritySite'] = false;
}

// Guardado de datos, envio de mail y respuesta ajax
if(($validar['fullname'] == true) AND ($validar['email'] == true) AND ($validar['codepais'] == true) AND ($validar['codearea'] == true) AND ($validar['telefono'] == true) AND ($validar['mensaje'] == true) AND ($validar['captcha'] == true) AND ($validar['prioritySite'] == true)){
    $conexion = conexion($bd_config);
    if(!$conexion){
        echo json_encode(['enviado' => "sinconexion"]);
    } else {

        date_default_timezone_set('America/Argentina/Buenos_Aires');
        $fechaRegistro = date("Y-m-d H:i:s");

        $geoip = geoIp();
        $pais_ip = htmlspecialchars($geoip -> geoplugin_countryName);
        $region_ip = htmlspecialchars($geoip -> geoplugin_region);
        $city_ip = htmlspecialchars($geoip -> geoplugin_city);
        
        $registrar = registrardatos($conexion, $fullname, $email, $codepais, $codearea, $telefono, $mensaje, $prioritySite, $fechaRegistro, $pais_ip, $region_ip, $city_ip);
        if ($registrar){
            correo_cliente($fullname, $email, $codepais, $codearea, $telefono, $mensaje, $prioritySite);
            correo_reporte($fullname, $email, $codepais, $codearea, $telefono, $mensaje, $prioritySite);
            echo json_encode(['enviado' => true, 'validacion' => $validar]);
        }else{
            echo json_encode(['enviado' => false]);
        }

    }
       
}else{
    echo json_encode(['enviado' => false, 'validacion' => $validar]);
}

?>