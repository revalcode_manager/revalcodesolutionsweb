<?php
# Funcion para conectarnos a la base de datos.
# Return: la conexion o false si hubo un problema.
function conexion($bd_config){
	$conexion = new mysqli($bd_config['servidor'], $bd_config['usuario'], $bd_config['pass'], $bd_config['basedatos']);
	if ($conexion->connect_errno){
		return false;
	} else {
		return $conexion;	
	}
}

# Filtros y limpieza.

function filtrobase ($dato){
	$dato = trim($dato);
	$dato = stripslashes($dato);
	$dato = htmlspecialchars($dato);
	return $dato;
}

function limpiarString($dato){
	$dato = filtrobase($dato);
	$dato = filter_var($dato, FILTER_SANITIZE_STRING);
	if (empty($dato)){
		$error = true;
	}else{
		$error = false;
	}

	$datos = array('dato' =>$dato , 'errores' =>$error);

	return $datos;
}

function limpiarNum($dato){
	$dato = filtrobase($dato);
	$dato = filter_var($dato,FILTER_SANITIZE_NUMBER_INT);
	if (empty($dato)){
		$error = true;
	}else{
		$error = false;
	}

	$datos = array('dato' =>$dato , 'errores' =>$error);

	return $datos;
}

function limpiarCorreo($dato){
	$dato= filtrobase($dato);
	$dato= filter_var($dato, FILTER_SANITIZE_EMAIL);

	IF(!filter_var($dato, FILTER_VALIDATE_EMAIL)){
		$error = true;
	} 
	if (empty($dato)){
		$error = true;
	}else if(!empty($dato) && $error == ''){
		$error = false;
	}

	$datos = array('dato' =>$dato , 'errores' =>$error);

	return $datos;

}

function listado_datos($fullname, $email, $codepais, $codearea, $telefono, $mensaje, $prioritySite){

	$lista = 
	'<li>Full Name: <strong>'.$fullname.'</strong></li>'.
	'<li>E-mail: <strong>'. $email.'</strong></li>'.
	'<li>Phone: <strong>'. $codepais .' '. $codearea .' ' . $telefono.'</strong></li>'.
	'<li>Messenger: <strong>'. $mensaje.'</strong></li>'.
	'<li>Priority for new website: <strong>'. $prioritySite .'</strong></li>';
	
	return $lista;		
}

function correo_reporte ($fullname, $email, $codepais, $codearea, $telefono, $mensaje, $prioritySite){
	// Mensaje para nosotros
	$messenger ='<html>'.
		'<head></head>'.
		'<body><p>De: Reval Code</p></br><p>Correo: hello@revalcodesolution.com</p></br>'.
		'<p><strong>¡Hola, nos an contactado!</strong></p></br>'.
		'<ul>'.
		$lista = listado_datos($fullname, $email, $codepais, $codearea, $telefono, $mensaje, $prioritySite).
		'</ul>'.
		'</br></body>'.
		'</html>';

	$enviar_a = 'fernogara@gmail.com';
	$asunto = 'Nueva contacto desde la pagina';
	$cabeceras = 'MIME-Version: 1.0' . "\r\n";
	$cabeceras .= 'Content-type: text/html; charset=utf-8' . "\r\n";
	mail($enviar_a, $asunto, $messenger, $cabeceras);
}

function correo_cliente ($fullname, $email, $codepais, $codearea, $telefono, $mensaje, $prioritySite){
	// Mensaje para el cliente
	$messenger ='<html>'.
		'<head></head>'.
		'<body><p>De: Reval Code</p></br><p>Correo: hello@revalcodesolution.com</p></br>'.
		'<p>¡Hello <strong>'.$fullname.'</strong>!</p>'.
		'<p>Your data was received successfully and we will contact you shortly.</p>'.
		'<p>These are the data we receive:</p></br>'.
		'<ul>'.
		$lista = listado_datos($fullname, $email, $codepais, $codearea, $telefono, $mensaje, $prioritySite).
		'</ul>'.
		'</br><p>Thank you <strong>'.$fullname.'</strong>for visiting our website.</p>'.
		'<p>Greetings atte.</p>'.
		'</body>'.
		'</html>';

	$enviar_a = $email;
	$asunto = 'My New Corp in Usa has received your message correctly';
	$cabeceras = 'MIME-Version: 1.0' . "\r\n";
	$cabeceras .= 'Content-type: text/html; charset=utf-8' . "\r\n";
	mail($enviar_a, $asunto, $messenger, $cabeceras);
}

// Validar Captcha

function validarCaptcha($captcha){

	$secret = "6LfECr0UAAAAAMetdJgkgWG6JkARfhN8t-IDZ8wc";
	$ip = $_SERVER['REMOTE_ADDR'];
	$respuestaValidacion = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=$secret&response=$captcha&remoteip=$ip");	
	$jsonResponde = json_decode($respuestaValidacion);
	if($jsonResponde->success){
		return true;	
	}else{
		return false;
	}
}

// Geo IP

function geoIp(){
	if ($_SERVER) {
		if ( $_SERVER[HTTP_X_FORWARDED_FOR] ) {
			$ip = $_SERVER["HTTP_X_FORWARDED_FOR"];
		} elseif ( $_SERVER["HTTP_CLIENT_IP"] ) {
			$ip = $_SERVER["HTTP_CLIENT_IP"];
		} else {
			$ip = $_SERVER["REMOTE_ADDR"];
		}
	} else {
		if ( getenv( "HTTP_X_FORWARDED_FOR" ) ) {
			$ip = getenv( "HTTP_X_FORWARDED_FOR" );
		} elseif ( getenv( "HTTP_CLIENT_IP" ) ) {
			$ip = getenv( "HTTP_CLIENT_IP" );
		} else {
			$ip = getenv( "REMOTE_ADDR" );
		}
	}

	$dataSolicitud = json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=" . $ip));
	return $dataSolicitud;
}

// Registrar en BD los datos

function registrardatos($conexion, $fullname, $email, $codepais, $codearea, $telefono, $mensaje, $prioritySite, $fechaRegistro, $pais_ip, $region_ip, $city_ip){
	$statement = $conexion->prepare("INSERT INTO form_registro (fullname, email, codepais, codearea, telefono, mensaje, prioritysite, fecha, paisip, regionip, cityip) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");		
	$statement->bind_param('sssiissssss', $fullname, $email, $codepais, $codearea, $telefono, $mensaje, $prioritySite, $fechaRegistro, $pais_ip, $region_ip, $city_ip);	
	$statement->execute();

	if($conexion->affected_rows < 1){
		$statement->close();
		return false;
	}else {
		$statement->close();
		return true;
	}
}