var form_contacto =document.getElementById('form_contacto'),
    fullname, email, telefono, mensaje, codepais, codearea, captcha, prioritySite,
    pais = document.getElementById('pais'),
    codPaisImput = document.getElementById('codepais'),
    contCodPais = document.getElementById('contCodPais'),
    validar = [],
    controlE = 0, controlF = 0,
    formularionew = document.getElementById( 'fs-form-wrap' );
    
function aplicarErrorCss(er){
    $('#' + er).parents('li').addClass('errorcss');
}

function quitarErrorCss(ok){
    $('#' + ok).parents('li').removeClass('errorcss');
}


function mostrarValidacion(r){
    if (!r['fullname']){
        aplicarErrorCss('fullname');
    }else{
        quitarErrorCss('fullname');
    }

    if(!r['email']){
        aplicarErrorCss('email');
    }else{
        quitarErrorCss('email');
    }

    if(!r['codepais']){
        aplicarErrorCss('codepais');
    }else{
        quitarErrorCss('ncodepais');
    }

    if(!r['codearea']){
        aplicarErrorCss('codearea');
    }else{
        quitarErrorCss('codearea');
    }

    if(!r['telefono']){
        aplicarErrorCss('telefono');
    }else{
        quitarErrorCss('telefono');
    }

    if(!r['mensaje']){
        aplicarErrorCss('mensaje');
    }else{
        quitarErrorCss('mensaje');
    }

    if(!r['prioritySite']){
        aplicarErrorCss('prioritySite');
    }else{
        quitarErrorCss('prioritySite');
    }

    if(!r['captcha']){
        $('#captcha').addClass('errorcss');
    }else{
        $('#captcha').removeClass('errorcss');
    }

}

//Agregar codigo de Paises
function agregarCodPais(e){
    e.preventDefault();
    var peticion = new XMLHttpRequest();
    peticion.open('POST', 'ajax/paises_code.json');
    peticion.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

    peticion.onreadystatechange = function(){
        if(peticion.readyState == 4 && peticion.status == 200){
            var r = JSON.parse(peticion.responseText);
            if(pais.value == "Choose option"){
                codPaisImput.value = "";
            }else{
                for(var i = 0; i < r.length; i++){
                    if(pais.value == r[i]["name"]){
                        codPaisImput.value = "+" + r[i]["phone_code"];
                        i = r.length;
                    }
                }
            }
        }
    }
    peticion.send();
}

//Verificar codigo de Paises
function VerificarCodPais(){
    var peticion = new XMLHttpRequest();
    peticion.open('POST', 'ajax/paises_code.json');
    peticion.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

    peticion.onreadystatechange = function(){
        if(peticion.readyState == 4 && peticion.status == 200){
            var r = JSON.parse(peticion.responseText);
            validar['codepais'] = false;
            for(var i = 0; i < r.length; i++){
                if(codepais == r[i]["phone_code"]){
                    validar['codepais'] = true;
                    i = r.length;
                }
            } 
        }
    }

    peticion.send();
}

//Agregar Paises al Option
function agregarPais(e){
    e.preventDefault();
    if (controlE == 0){
        controlE = 1;
        var peticion = new XMLHttpRequest();
        peticion.open('POST', 'ajax/paises_code.json');
        peticion.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

        peticion.onreadystatechange = function(){
            if(peticion.readyState == 4 && peticion.status == 200){
                var r = JSON.parse(peticion.responseText);
                pais.innerHTML = ("<option value='' disabled selected>Select Country</option>");
                for(var i = 0; i < r.length; i++){
                    pais.innerHTML += ("<option value='"+ r[i]["name"] +"'>" + r[i]["name"] + "</option>");
                }
            }
        }

        peticion.send();
    }
}

function largominmax(dato, min, max){
    if((dato.length < max) && (dato.length > min)){
        return true;
    }else{
        return false;
    }
}

function validardato(dato, eRegular){
    if(dato == ''){
        return false;
    } else if(eRegular.test(dato)){
        return true 
    }else{
        return false;
    }
}

function formulario_valido(){

    validar['fullname'] = validardato(fullname, /^([A-Za-zñÑáÁéÉíÍóÓúÚ ])*$/);
    validar['email'] = validardato(email, /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/);
    validar['codepais'] = validardato(codepais, /^([0-9])*$/);
    if (validar['codepais'] == true){
        VerificarCodPais();  
    }
    validar['codearea'] = validardato(codearea, /^([0-9])*$/);
    validar['telefono'] = validardato(telefono, /^([0-9])*$/);
    validar['mensaje'] = largominmax(mensaje, -1, 250);

    if (prioritySite == "Market Page"){
        validar['prioritySite'] = true;
    }else{
        if (prioritySite == "Web Site"){
            validar['prioritySite'] = true;
        }else{
            if (prioritySite == "Landing Page"){
                validar['prioritySite'] = true;
            }else{
                validar['prioritySite'] = false;
            }
        }
    }

    if (captcha == ""){
        validar['captcha'] = false;
    }else{
        validar['captcha'] = true;
    }
    
    if (validar['fullname']){
        if (validar['email']){
            if (validar['codepais']){
                if (validar['codearea']){
                    if (validar['telefono']){
                        if (validar['mensaje']){
                            if (validar['captcha']){
                                if (validar['prioritySite']){
                                    validar['formulario'] = true;
                                }else{
                                    validar['formulario'] = false;
                                }
                            }else{
                                validar['formulario'] = false;
                            }
                        }else{
                            validar['formulario'] = false;
                        }
                    }else{
                        validar['formulario'] = false;
                    }
                }else{
                    validar['formulario'] = false;
                }
            }else{
                validar['formulario'] = false;
            }
        }else{
            validar['formulario'] = false;
        }
    }else{
        validar['formulario'] = false;
    }
    
    return validar;
}

function envio_form(e){
    e.preventDefault();
    
    fullname = form_contacto.fullname.value.trim();
	email = form_contacto.email.value.trim();
    codepais = form_contacto.codepais.value.trim();
    codepais = codepais.replace("+","");
    codearea = form_contacto.codearea.value.trim();
    telefono = form_contacto.telefono.value.trim();
    mensaje = form_contacto.mensaje.value.trim();
    captcha = document.getElementById('g-recaptcha-response').value;
    prioritySite = form_contacto.prioritySite.value.trim();

    validar = formulario_valido();
    if(validar['formulario']){
        $('.fs-message-error-end').attr({'style': 'display: none'});

        var parametros = 'fullname='+ fullname + '&email='+ email +'&telefono=' + telefono +'&codepais=' + codepais +'&codearea=' + codearea +'&mensaje=' + mensaje +'&prioritySite=' + prioritySite +'&captcha=' + captcha;
        var peticion = new XMLHttpRequest();
        peticion.open('POST', 'ajax/procesar.form.php');
        peticion.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		peticion.onreadystatechange = function(){
			if(peticion.readyState == 4 && peticion.status == 200){
                var r = JSON.parse(peticion.responseText);
                if(r['enviado'] == true){
                    $('.fs-message-error-end').attr({'style': 'display: none'});
                    mostrarValidacion(r['validacion']);
                    $(".mail-btn").removeClass("fs-submit");
                    $(".mail-btn").addClass("fly");
                    that = this
                    setTimeout(function() {
                        $(that).removeClass("fly");
                    }, 5400);
                    setTimeout(function() {
                        $('#modalfull').modal('toggle');
                    }, 1500);
                    setTimeout(function() {
                        location.reload();
                    }, 2500);

                }else{
                    if (r['enviado'] == 'sinconexion'){
                        header('Location: ../error.php');
                    }else{
                        if (r['enviado'] == false){
                            $('.fs-message-error-end').attr({'style': 'display: block'});
                            mostrarValidacion(r['validacion']);
                        }
                    }
                }
			}
		}

		peticion.send(parametros);
		
	} else {
        $('.fs-message-error-end').attr({'style': 'display: block'});
        mostrarValidacion(validar);
	}

}

function iniciarModal(){
    if (controlF === 0){
        controlF = 1;
    
        formWrap = formularionew;

        [].slice.call( document.querySelectorAll( 'select.cs-select' ) ).forEach( function(el) {	
            new SelectFx( el, {
                stickyPlaceholder: false,
                onChange: function(val){
                    document.querySelector('span.cs-placeholder').style.backgroundColor = val;
                }
            });
        } );

        new FForm( formWrap, {
            onReview : function() {
                classie.add( document.body, 'overview' ); // for demo purposes only
            }
        } );
    }
}

form_contacto.addEventListener('submit', function(e){
	envio_form(e);
});

$( "#modalActivate" ).click(function() {
    iniciarModal();
});

pais.addEventListener('change', function(e){
    agregarCodPais(e);
});

pais.addEventListener('click', function(e){
    agregarPais(e);
});

$('#modalfull').on('show.bs.modal', function (e){
    $('#bodyCode').attr({'style': 'overflow: hidden !important;'});
});

$('#modalfull').on('hide.bs.modal', function (e){
    $('#bodyCode').attr({'style': 'overflow: auto !important;'});
});